package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{    
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;
   
   private static final double DEFAULT_RATE = 10;

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JPanel panel;
   private JLabel resultLabel = new JLabel("Balance: ");
   
   public InvestmentFrame()
   { 
      // Use helper methods 
      createTextField();
      createButton();
      createPanel();
      
      setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
   }

   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   private void createButton()
   {
      button = new JButton("Add Interest");
   }
   
   public String getTextRatefield()
   {
	   return rateField.getText();
   }
   
   public void setTextResultLabel(String Balance)
   {
	   resultLabel.setText(Balance);
   }
   

   private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   } 
   
   public void setButtonListener(ActionListener listener){
	   button.addActionListener(listener);
   }
}
