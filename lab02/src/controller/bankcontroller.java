package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import view.InvestmentFrame;
/*
 * เพราะต้องใช้object ของ เฟรมและ Bankaccount มาทำงานร่วมกัน โดยให้controllerทำงานเหมือนตัวควบคุม
*/
public class bankcontroller {
	private BankAccount account = new BankAccount(INITIAL_BALANCE);;
	InvestmentFrame frame;
	private static final double INITIAL_BALANCE = 1000;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new bankcontroller();

	}
	
	public bankcontroller()
	{
		frame = new InvestmentFrame();
		frame.setButtonListener(new InterestListener());
	}
	
    class InterestListener implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {
          double rate = Double.parseDouble(frame.getTextRatefield());
          double interest = account.getBalance() * rate / 100;
          account.deposit(interest);
          frame.setTextResultLabel("balance: " + account.getBalance());
       }            
    }
}
